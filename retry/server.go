package main

import (
    "log"
    "net"
    "net/http"
    "net/rpc"
    "time"
    "errors"
    "os"
    "strconv"
)

type Args struct {
    Instant time.Time
}

type Server struct {
    last time.Time
    count int64
    reject bool
    lag time.Duration
}

func (s *Server) Serve(args *Args, reply *bool) error {
    if s.reject {
        *reply = false
        return errors.New("Serve refused")
    }

    now := time.Now()
    lag := now.Sub(args.Instant)

    if time.Since(s.last).Seconds() > 1.0 {
	log.Printf("%s RPS: %d Lag: %s", s.last, s.count, s.lag)
        s.last = now
        s.count = 1
	s.lag = time.Duration(0)
    } else {
        s.count++
	if lag > s.lag {
            s.lag = lag
	}
    }

    *reply = true
    return nil
}

func main() {
    before_downtime, e := strconv.Atoi(os.Args[1])
    if e != nil {
        log.Fatal("parse error:", e)
    }
    downtime, e := strconv.Atoi(os.Args[2])
    if e != nil {
        log.Fatal("parse error:", e)
    }
    server := new(Server)
    rpc.Register(server)
    rpc.HandleHTTP()
    listen, e := net.Listen("tcp", ":")
    if e != nil {
        log.Fatal("listen error:", e)
    }
    port := listen.Addr().String()
    log.Printf("Listening on addr %s", port)

    go func() {
        time.Sleep(time.Second * time.Duration(before_downtime))
        log.Printf("Close connection")
        server.reject = true

        time.Sleep(time.Second * time.Duration(downtime))
        log.Printf("Reopen connection")
        server.reject = false
    }()

    http.Serve(listen, nil)
}
