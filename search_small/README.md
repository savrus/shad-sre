Одна из часто встречающихся задач в программах - поиск элемента в множестве. В случае когда у нас числа и они хранятся в массиве, стандартное решение - двоичный поиск. Во многих языках программирования, в том числе и C++, двоичный поиск реализован в библиотеке. В случае C++ - `std::find()`.

В ряде случаев, стандартная реализация неэффективна. Например, если массив небольшого размера, накладные расходы стандартного решения становятся заметны. Подумайте, чем же неоптимален двоичный поиск для современных CPU? Как можно численно измерить и подтвердить или опровергнуть вашу гипотезу? Используйте утилиту `perf` и счётчики производительности.

Подумайте, как может выглядеть решение без этих накладных расходов. Реализуйте быстрый поиск по маленькому отсортированному массиву (меньше 100 интов). 
Напишите своё решение в файле `search.cpp`.

